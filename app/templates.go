package app

import (
	"io"
	"path/filepath"
	"strings"
	"text/template"

	"github.com/labstack/echo/v4"
)

var templates map[string]*template.Template

type Template struct {
	templates *template.Template
}

func (t *Template) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	tmpl, ok := templates[name]
	if !ok {
		println("template not found: ", name)
	}
	err := tmpl.ExecuteTemplate(w, "layout", data)
	if err != nil {
		println(err.Error())
	}
	return err
}

func init() {
	ReloadTemplates()
}

func ReloadTemplates() {
	if templates == nil {
		templates = make(map[string]*template.Template)
	}

	layouts, err := filepath.Glob("./app/templates/layouts/*.html")
	if err != nil {
		panic("Failed to glob templates")
	}

	pages, err := filepath.Glob("./app/templates/pages/*.html")
	if err != nil {
		panic("Failed to glob templates")
	}

	var globalShared []string

	for _, layout := range layouts {
		for _, page := range pages {
			files := append(globalShared, layout, page)
			layoutBase := filepath.Base(layout)
			layoutShort := layoutBase[0:strings.LastIndex(layoutBase, ".")]
			pageBase := filepath.Base(page)
			pageShort := pageBase[0:strings.LastIndex(pageBase, ".")]
			templates[layoutShort+":"+pageShort] = template.Must(template.New(pageShort).ParseFiles(files...))
		}
	}
	println("Reloaded templates!")
}
