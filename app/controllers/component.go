package controllers

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/pontaoski/buggsy/app/database"
	"github.com/pontaoski/buggsy/app/models"
)

// path: /component/:id
func ComponentAction(c echo.Context) error {
	var component models.Component
	var bugs []models.Bug
	database.Db.Where("id = ?", c.Param("id")).First(&component)
	database.Db.Model(&component).Related(&bugs)
	component.Bugs = bugs
	return c.Render(http.StatusOK, "base:component", component)
}
