package controllers

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/pontaoski/buggsy/app/database"
	"github.com/pontaoski/buggsy/app/models"
)

// path: /products
func ProductListAction(c echo.Context) error {
	var products []models.Product
	database.Db.Find(&products)
	return c.Render(http.StatusOK, "base:products", products)
}

// path: /product/:id
func ProductAction(c echo.Context) error {
	var product models.Product
	var components []models.Component
	database.Db.Where("id = ?", c.Param("id")).First(&product)
	database.Db.Model(&product).Related(&components)
	product.Components = components
	return c.Render(http.StatusOK, "base:product", product)
}
