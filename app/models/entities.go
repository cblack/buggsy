package models

import (
	"github.com/jinzhu/gorm"
)

type SeverityType int

const (
	Normal SeverityType = iota
	Critical
	Grave
	Major
	Crash
	Minor
	Wishlist
	Task
)

type BugStatusType int

const (
	Reported BugStatusType = iota
	Confirmed
	Assigned
	Resolved
	NeedInfo
)

type Component struct {
	gorm.Model
	Name        string
	Description string `gorm:"type:text"`
	Bugs        []Bug
	ProductID   uint
}

type Product struct {
	gorm.Model
	Name        string
	Description string `gorm:"type:text"`
	Active      bool
	Components  []Component
}

type Comment struct {
	gorm.Model
	Text      string `gorm:"type:text"`
	CreatorID uint
	BugID     uint
}

type Bug struct {
	gorm.Model
	Severity    SeverityType
	BugStatus   BugStatusType
	ProductID   uint
	ComponentID uint
	Comments    []Bug
	Depends     []*Bug `gorm:"many2many:blocks;association_jointable_foreignkey:depend_id"`
	Name        string
	Description string
	CreatorID   uint
}

type User struct {
	gorm.Model
	Email    string
	FullName string
	Bugs     []Bug     `gorm:"foreignkey:CreatorID"`
	Comments []Comment `gorm:"foreignkey:CreatorID"`
}
