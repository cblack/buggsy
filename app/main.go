package app

import (
	//"crypto/subtle"

	"os"
	"path/filepath"

	"github.com/jinzhu/gorm"
	"github.com/pontaoski/buggsy/app/controllers"
	"github.com/pontaoski/buggsy/app/database"
	"github.com/pontaoski/buggsy/app/models"

	"github.com/fsnotify/fsnotify"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/log"
)

func Main() {
	var err error

	database.Db, err = gorm.Open("sqlite3", "bugs.db")
	if err != nil {
		log.Fatal(err)
	}
	defer database.Db.Close()

	database.Db.AutoMigrate(&models.User{})
	database.Db.AutoMigrate(&models.Product{})
	database.Db.AutoMigrate(&models.Bug{})
	database.Db.AutoMigrate(&models.Comment{})
	database.Db.AutoMigrate(&models.Component{})

	watcher, _ := fsnotify.NewWatcher()
	defer watcher.Close()

	if err := filepath.Walk("./app/templates/", func(path string, info os.FileInfo, err error) error {
		if err != nil {
			println(err.Error())
		}
		if info.Mode().IsDir() {
			return watcher.Add(path)
		}
		return nil
	}); err != nil {
		panic("Failed to watch directory: " + err.Error())
	}

	go func() {
		for {
			select {
			case <-watcher.Events:
				ReloadTemplates()
			}
		}
	}()

	app := echo.New()
	app.Logger.SetLevel(log.DEBUG)
	app.Renderer = &Template{}
	app.GET("/products", controllers.ProductListAction)
	app.GET("/product/:id", controllers.ProductAction)
	app.GET("/component/:id", controllers.ComponentAction)
	app.Static("/static", "dist")
	app.Logger.Fatal(app.Start(":8000"))
}
