const autoprefixer = require('autoprefixer');
const ManifestPlugin = require('webpack-manifest-plugin');

const production = process.env.NODE_ENV === 'production';
const host = process.env.HOST || 'localhost'
const devServerPort = 8080;

module.exports = {
    entry: ['./assets/app.scss', './assets/app.js'],
    output: {
        filename: 'bundle.js',
    },
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: 'bundle.css',
                        },
                    },
                    { loader: 'extract-loader' },
                    { loader: 'css-loader' },
                    {
                        loader: 'postcss-loader',
                        options: {
                            plugins: () => [autoprefixer()]
                        }
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            // Prefer Dart Sass
                            implementation: require('sass'),
                            sassOptions: {
                                includePaths: ['./node_modules'],
                            },
                        },
                    }
                ],
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                query: {
                    presets: ['@babel/preset-env'],
                },
            }
        ],
    },
    plugins: [
        new ManifestPlugin({
            writeToFileEmit: true,
            publicPath: production ? "/webpack/" : 'http://' + host + ':' + devServerPort + '/webpack/',
        }),
    ]
};